(ns schnestiny.endpoints.destiny2
    (:require-macros [cljs.core.async.macros :refer [go]])
    (:require [cljs-http.client :as http]
              [cljs.core.async :refer [<!]]
              [clojure.string :as s]
              [schnestiny.endpoints :refer [api-root api-key]]))

(defn search-destiny-player-endpoint [membership-type display-name]
    (s/join [api-root "Destiny2/SearchDestinyPlayer/" membership-type "/" (s/replace display-name "#" "%23") "/"]))

(defn search-destiny-player [membership-type display-name]
    (http/get (search-destiny-player-endpoint membership-type display-name)
              {:with-credentials? false
               :headers           {"x-api-key" api-key}}))