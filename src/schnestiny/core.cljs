(ns schnestiny.core
    (:require-macros [cljs.core.async.macros :refer [go]])
    (:require [cljs.core.async :refer [<!]]
              [reagent.core :as r]
              [schnestiny.endpoints.destiny2 :refer [search-destiny-player]]))

;; -------------------------
;; Views

(def state (r/atom {}))

(go
    (reset! state
            (get-in (<! (search-destiny-player -1 "VoxlDavid#2189"))
                    [:body :Response])))

(defn user-card [card]
    (map (fn [a b] (println a) (println b) [:div (name a) ": " (str b)]) (keys card) (vals card)))

(defn home-page []
    (apply conj [:div] (map user-card @state)))

;; -------------------------
;; Initialize app

(defn mount-root []
    (r/render [home-page] (.getElementById js/document "app")))

(defn init! []
    (mount-root))
